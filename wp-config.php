<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '811iZkYyC6EEclWoEjFTIUXyxgKRSnV2Si20SZPZodGoJslDOzkIOdu5+LXtJT1GXCrl3Q6+rgUSuau+LU9cCg==');
define('SECURE_AUTH_KEY',  'Xfc/j6GFSm9i5LisCvtX2Q/QXFS8zPLVHhQhisHmIW5UfvibYHwrk+BYdnHt0MTPOfPEeztxE6SadeOO1/gMuA==');
define('LOGGED_IN_KEY',    'vxHxTx0K8QIO1k2mNBJDiS1kUNcBfay+pCT0TWQ0QSj48LdeNrahfJYVMZWZ3iKTLk06erRAMsmcww3fW3H4MA==');
define('NONCE_KEY',        'hKBRiVAM9V4eG3QqT6CFFtSrvx6BZEd0nsi3ngRWRQ474N0eGHw+X4XKk2BAXjfK6DmyfoZ1V+OvcrT4QcTF3A==');
define('AUTH_SALT',        'ZOXVAm5/Drpptf5Q+krpzqXPK/m6KkiPXrmYiCoalpzBZQPhIS+oWO5scgPyjOv/Qm32AkecjvsJga2oMiUQMQ==');
define('SECURE_AUTH_SALT', 'nMs01dDpmQy4VXywLZSSGUn2sV/bT+e6681dpisTCITtm/CSyE6JSZhfhkXEpzjeIql/1kZSQUCH9/+QYCLyIA==');
define('LOGGED_IN_SALT',   '0sVQCmPPktk7MjEgTQsVEI2Cp8K5m3cONzCXWPoqUPwe6n1W8+QkSraGPc64Us6BrKjdOD+twwrWnRwCr+pxGA==');
define('NONCE_SALT',       'GuEP2+fFjPwuYgZxkpTVBuEdxiyU/3FWhJY6FslR0kMv13QjdFG6dEULiCi/umvdcgOFl6xE40UDR9Y2a89DPw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';





/* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ) {
	$_SERVER['HTTPS'] = 'on';
}

/* Inserted by Local by Flywheel. Fixes $is_nginx global for rewrites. */
if ( ! empty( $_SERVER['SERVER_SOFTWARE'] ) && strpos( $_SERVER['SERVER_SOFTWARE'], 'Flywheel/' ) !== false ) {
	$_SERVER['SERVER_SOFTWARE'] = 'nginx/1.10.1';
}
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
