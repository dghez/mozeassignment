{{--
  Template Name: Custom Landing
--}}

@php
	// ACF 
	$benefits  = get_field('benefits'); // why choose group

@endphp


@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())


	<div class="bgFull bgFull--darkBlue">
		<div id="container-p5"></div>
		<section class="container-fluid hero hero--landing">
			<div class="row align-items-center">
				<div class="col-md-7">
					<h1><span class="highlighted">Cryptocurrencies</span><br><span> made easy</span></h1>
					<h2>Inserisci i contanti. Preleva <span class="highlighted">Bitcoin</span> <br class="d-none d-md-block">È davvero così semplice</h2>
					<a class="cta cta--blue">Trova un ATM vicino a te</a>
					<a class="hero-link f-h6" href="http://www.coinbase.com" target="_BLANK">Oppure <span>compra Bitcoin online</span></a>
				</div>
			</div>
		</section>
	</div>


	<section class="container-fluid section-product">

		{{-- Below hero --}}
		<div class="row">
			<div class="col-auto">
				<span class="label">Prodotti</span>
			</div>
			<div class="w-100"></div>
			<div class="col-md-7">
				<h3><span class="highlighted">Semplici</span> e <span class="highlighted">sicuri</span>.<br class="d-none d-md-block">I nostri ATM sono il passaporto più facile per il mondo delle cryptovalute </h3>
			</div>
			<div class="col-md-5">La registrazione è pienamente trasparente e sicura. Tramite un processo di KYC (Know Your Customer), viene effettuata un’autenticazione a 3 fattori direttamente sull’ATM (numero di telefono, codice OTP inviato via SMS, documento di identità, foto volto).</div>
		</div>

		{{-- Steps --}}
		<div class="row text-center steps-container box">
			<div class="box--grey"></div>
			<div class="col-12 box-title"><h4>Quattro semplici passaggi.<br class="d-none d-md-block">Il processo di registrazione più veloce</h4></div>
			<div class="col-md-3">
    			@include('partials.svg.steps.insertCode')
				<div class="smallTitle">Inserisci il tuo numero<br class="d-none d-md-block"> di cellulare</div>
			</div>
			<div class="col-md-3">
    			@include('partials.svg.steps.insertNumber')
				<div class="smallTitle">Inserisci il codice OTP<br class="d-none d-md-block"> ricevuto via SMS</div>
			</div>
			<div class="col-md-3">
    			@include('partials.svg.steps.scanId')
				<div class="smallTitle">Scansiona il tuo<br class="d-none d-md-block"> documento di identità</div>
			</div>
			<div class="col-md-3">
    			@include('partials.svg.steps.selfie')
				<div class="smallTitle">Scatta una fotografia del<br class="d-none d-md-block"> tuo volto</div>
			</div>
		</div>

		{{-- Install --}}
		<div class="row align-items-center install-container">
			<div class="col">
				<h4 class="d-inline-block">Vuoi installare uno dei nostri ATM nella tua attività commerciale?</h4>
			</div>
			<div class="col-auto ml-auto">
				<span class="cta cta--white">Contattaci ora</span>
			</div>
		</div>
	</section>


	<div class="bgFull--gradientBlue">
		<section class="container-fluid section-benefits">
			<div class="benefit-circles">
				@include('partials.svg.benefit-circles')				
			</div>
			<div class="row">
				<div class="col-12 text-center"><img class="icon--section" src="@asset('images/icons/icon--rocket.svg')" alt="icon-rocket"></div>
				<div class="col-12 text-center"><span class="label">Benefit</span></div>
				<div class="col-12 text-center">
					<h3>Perché dovrei scegliere <span class="highlighted">Bitcoin</span>?</h3>
				</div>
				<div class="col-12">
					<div class="row box">
						<div class="box--grey"></div>
						@foreach($benefits['benefit'] as $single)
							<div class="col-md-4">
								<h4 class="highlighted">{!! $single['title'] !!}</h4>
								<p>{!! $single['description'] !!}</p>
							</div>
						@endforeach
					</div>
				</div>

			</div>
		</section>		
	</div>


	<section class="container-fluid section-team" id="section-team">
		<div class="row">
			<div class="col-12 section-team-bigText">Bitcoin team</div>
			
		</div>
	</section>

  @endwhile
@endsection
