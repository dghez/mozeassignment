<div class="bgFull--darkBlue">
	<footer class="content-info">
			<section class="container-fluid">
				<div class="row align-items-center footer-nav">
					<div class="col-auto">
						<img src="@asset('images/brandLogo.svg')" alt="">
					</div>
					<div class="col-auto ml-auto">
						<ul>
							<a href="#"><li class="smallText">Prodotti</li></a>
							<a href="#"><li class="smallText">Chi siamo</li></a>
							<a href="#"><li class="smallText">Mappa atm</li></a>
							<a href="#"><li class="smallText">Contatti</li></a>
						</ul>
					</div>
				</div>
	
				<div class="divider"></div>
				<div class="row footer-info">
					<div class="col-md-4"><div class="footer-info-title">Indirizzo</div>Via Merano, 16 <br>20127 — Milano, Italy</div>
					<div class="col-md-4"><div class="footer-info-title">Contattaci</div><a href="mailto:hello@bitcoin.com">hello@bitcoin.com</a><br><a href="tel:00393450000000">+ 39 345 000 00 00</a></div>
					<div class="col-md-auto ml-md-auto footer-social">
						<div class="footer-info-title">Seguici</div>
						<a href="#">@include('partials.svg.icon--facebook')</a>
						<a href="#">@include('partials.svg.icon--linkedin')</a>
						<a href="#">@include('partials.svg.icon--twitter')</a>
					</div>
				</div>
				<div class="divider"></div>

				<div class="row footer-copyright f-smallText">
					<div class="col">
						<a href="#">Termini e condizioni</a>
						<a href="#">Privacy policy</a>
					</div>
					<div class="col-md-auto ml-auto">
						Copyright © 2017 All rights reserved — Designed by <a href="http://www.moze.com">Moze</a>
					</div>
				</div>
			</section>
	</footer>
</div>
