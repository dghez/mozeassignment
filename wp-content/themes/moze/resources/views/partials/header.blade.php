<header class="banner">
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="col-auto">
				<a class="brand" href="{{ home_url('/') }}"><img src="@asset('images/brandLogo.svg')" alt=""></a>
			</div>
			<div class="col-auto ml-auto">
				<nav class="nav-primary">
					@if (has_nav_menu('primary_navigation'))
						{!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
					@endif
				</nav>
			</div>
			<div class="col-auto ml-auto">
				<div class="btn btn--secondary">Compra Bitcoin Online</div>
			</div>
		</div>	 
	</div>
</header>
