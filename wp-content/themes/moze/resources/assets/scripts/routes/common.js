import TweenMax from 'gsap'; // eslint-disable-line no-unused-vars
import ScrollMagic from 'scrollmagic';  // eslint-disable-line no-unused-vars
import 'animation.gsap'; // eslint-disable-line no-unused-vars

export default {
	init() {
		// JavaScript to be fired on all pages
	},
	finalize() {
		// JavaScript to be fired on all pages, after page specific JS is fired
		var controller = new ScrollMagic.Controller();

		var headerHandles = new ScrollMagic.Scene({ // eslint-disable-line no-unused-vars
			offset: 10,
		})
		.setClassToggle('header', 'bg-active')
		.addTo(controller);


	},
};
