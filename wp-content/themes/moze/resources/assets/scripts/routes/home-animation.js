import p5 from 'p5';  // eslint-disable-line no-unused-vars
import {TweenMax, TimelineMax} from 'gsap';  // eslint-disable-line no-unused-vars
var btcPath = require('../../images/btcLogo.jpg');
var ltcPath = require('../../images/ltcLogo.jpg');
// var ethPath = require('../../images/ethLogo.jpg');
// var scamPath = require('../../images/scam2.jpg');

export default {
	init() {
		// JavaScript to be fired on the home page
	},
	finalize() {
		// JavaScript to be fired on the home page, after the init JS




		var sketch = function(p){

			var windowWidth = $(window).width();
			var windowHeight = $(window).height();
			var tl = new TimelineMax();

			/* eslint-disable */
			p5.disableFriendlyErrors = true;
			var btc;
			var Stars = [];
			var smallSize = 80;
			var scaleFactor = (windowWidth * 0.4) / smallSize;
			var starsColor = ['#0d2a75', '#1d5cd2', '#051247'];
			var translate = {
				x: windowWidth / 2,
				y: (windowHeight - (windowWidth * 0.4)) / 2
			};
			var positions = {
				'btc' : [],
				'ltc' : [],
				'eth' : []
			};

			function translatePoint(vector){
				var newX = (vector.x * scaleFactor) + translate.x;
				var newY = (vector.y * scaleFactor) + translate.y;
				var newVec = p.createVector(newX, newY);
				return newVec;
			}


			var center = translatePoint(p.createVector(smallSize / 2, smallSize / 2));

			var bgColor = p.color('#000529');


			function Star(vector){
				this.x = vector.x;
				this.y = vector.y;
				this.pos = p.createVector(p.random(windowWidth * 1.2), p.random(windowHeight * 1.2)); // initial position
				this.target = p.createVector(this.x,this.y);
				this.color = p.random(starsColor);
				this.size = p.random(2,4);
				this.vel = p.createVector(p.random(-5,6), p.random(-4,6));
				this.acc = p.createVector();
				this.maxSpeed = 10;
				this.maxForce = 2;
				this.weight = p.random(0.5, 2.5);
				this.allowRepulse = false;
				this.goingCenter = false;

				this.alpha;
			}

			Star.prototype.update = function(){
				this.pos.add(this.vel);
				this.vel.add(this.acc);
				this.alpha;
				this.acc.mult(0);
			}

			Star.prototype.move = function(){
				var attract = this.attract(this.target);
				this.appForce(attract);

				var mouse = p.createVector(p.mouseX, p.mouseY);
				var repulse = this.repulse(mouse);
				this.appForce(repulse);

			}

			Star.prototype.appForce = function(value){
				this.acc.add(value);
			}

			Star.prototype.modAlpha = function(){
				var desired = p5.Vector.sub(this.target, this.pos);
				var d = desired.mag();
				var opacity = p.map(d, 0, 800, 255, 0);
				return opacity;
			}
			
			Star.prototype.attract = function(target){
				var desired = p5.Vector.sub(target, this.pos);
				var d = desired.mag(); // distance between target and position
				if( d < 100 && !this.goingCenter){
					this.vel.limit(10);
					if( d < 10 && !(this.allowRepulse)){
						var G = 70;
						var distancesq = desired.magSq();
					
						var force = distancesq / G;
						desired.setMag(force * this.weight);
						this.vel.limit(0.7);
						return desired;
					}
				} else {
					var G = 400000;
					var distancesq = desired.magSq();
					var force = G / distancesq;
					desired.setMag(force);
					this.vel.limit(7);
					return desired;
				}
			}

			Star.prototype.repulse = function(target){
				var desired = p5.Vector.sub(target, this.pos);
				var d = desired.mag();
				if(d < 80) {
					// this.vel.limit(20);
					this.allowRepulse = true;
					// desired.setMag(this.maxSpeed);
					// desired.mult(p.random(-2,-1));
					// var force = p5.Vector.sub(desired, this.vel);
					var distancesq = desired.magSq();
					var force = (500 / distancesq) * this.weight;
					// force.limit(this.maxForce * 1.1  * this.weight);
					desired.setMag(-force);
					return desired;					
				}
				else {
					this.allowRepulse = false;
					return p.createVector(0,0);
				}
			}

			Star.prototype.render = function(){
				p.noStroke();
				var color = p.color(this.color);
				color.setAlpha(this.modAlpha());
				p.fill(color);
				p.ellipse(this.pos.x, this.pos.y, this.size, this.size);					
			}


			p.setup = function(){
				p.createCanvas(windowWidth, windowHeight, 'WEBGL');
				p.pixelDensity(1);
				p.background(bgColor);
				var btc = p.loadImage(btcPath, function(img) {
					// p.image(img, 0, 0);
					btc.resize(smallSize,smallSize);
					btc.loadPixels();
					for( var x=0; x < btc.width; x++){
						for( var y=0; y < btc.height; y++){
							var index = (x + y * btc.width) * 4;
							if(btc.pixels[index] < 200){ // if channel red < 200 == black in this case
								if(Math.round(p.random(100)) < 70){ //get only % of the points, randomly
									var initVec = p.createVector(x,y);
									var pos = translatePoint(initVec);
									positions.btc.push(pos);
									Stars.push(new Star(pos));									
								}
							}
						}
					}
				}); // close callback loader image
				var ltc = p.loadImage(ltcPath, function(img) {
					// p.image(img, 0, 0);
					ltc.resize(smallSize,smallSize);
					ltc.loadPixels();
					for( var x=0; x < ltc.width; x++){
						for( var y=0; y < ltc.height; y++){
							var index = (x + y * ltc.width) * 4;
							if(ltc.pixels[index] < 200){ // if channel red < 200 == black in this case
								if(Math.round(p.random(100)) < 70){ //get only % of the points, randomly
									var initVec = p.createVector(x,y);
									var pos = translatePoint(initVec);
									positions.ltc.push(pos);
								}
							}
						}
					}
				}); // close callback loader image
				console.log(positions);



			
				p.pixelDensity();				

			}



			p.draw = function(){
				p.background(bgColor);
				for(var k = 0; k < Stars.length; k++){
					Stars[k].move();					
					Stars[k].update();					
					Stars[k].render();
				}	
			}


			p.windowResized = function(){
				windowWidth = $(window).width();
				windowHeight = $(window).height();
				p.resizeCanvas(windowWidth, windowHeight);
			}

			tl
				.to('.yo', 10, {
					onComplete: function(){
						for(var i = 0; i < Stars.length; i++){
							Stars[i].vel.limit(50);
							Stars[i].goingCenter = true;
							Stars[i].target = center;
						}
					}})

				.to('.yo', 2, {onComplete: function(){
					for(var i = 0; i < positions.ltc.length; i++){
						if (i >= Stars.length) {
							Stars.push(new Star(positions.ltc[i]));
							Stars[i].pos = p.createVector(center.x,center.y);
						}

						Stars[i].target = positions.ltc[i];
						Stars[i].goingCenter = false;
					}
				}})



		};

		var yo = new p5(sketch, window.document.getElementById('container-p5'));

		/* eslint-enalbe */
	

	},
};
