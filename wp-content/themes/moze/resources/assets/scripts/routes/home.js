import {TweenMax, TimelineMax, Power0, Power4, Power3, Power1, Power2, yoyo} from 'gsap';  // eslint-disable-line no-unused-vars
import ScrollMagic from 'scrollmagic';  // eslint-disable-line no-unused-vars
import 'animation.gsap'; // eslint-disable-line no-unused-vars
import opening from '../support/opening.js';  // eslint-disable-line no-unused-vars

var iconLinkedin = require('../../images/icons/icon--linkedin.svg');
var iconTwitter = require('../../images/icons/icon--twitter.svg');


export default {
	init() {
		// JavaScript to be fired on the home page
	},
	finalize() {
		// JavaScript to be fired on the home page, after the init JS

		// INIT CANVAS
		setTimeout(function(){
			opening.init();
		}, 1000);



		// GET THE TEAM MEMBER
		$.ajax({
			url: 'https://www.mocky.io/v2/5a70d62e3300000750ff5e33',
			type: 'GET',
			dataType: 'JSON',
		})
		.done(function(data) {
			data.users.forEach(function(el){
				var teamMember = 
					'<div class="col-md-3">' + // we know they are just 4
						'<div class="team-member-container">' + 
							'<img class="team-member-photo" src="' + el.avatar + '" alt="" />' + 
							'<div class="team-member-info">' + 
								'<h4>' + el.first_name + ' ' + el.last_name + '</h4>' + 
								'<div>' + el.role + '</div>';

								if(el.hasOwnProperty('twitter')) {
									if(el.twitter !== ''){
										teamMember += '<a href="' + el.twitter + '" target="_BLANK"><img src="' + iconTwitter + '" alt="" /></a>';
									}
								}

								if(el.hasOwnProperty('linkedin')) {
									if(el.linkedin !== ''){
										teamMember += '<a href="' + el.linkedin + '" target="_BLANK"><img src="' + iconLinkedin + '" alt="" /></a>';	
									}
								}
									
							teamMember  += '</div>';
						teamMember  += '</div>';
					teamMember  += '</div>';

				$('#section-team .row').append(teamMember);
			})
			revealTeamScroll();

		})
		.fail(function() {
			$('#section-team').remove();
		})



		// OPENING ANIMATION
		var tlOpening = new TimelineMax({ 
			onStart: function(){
				TweenMax.fromTo('#container-p5', 10, {autoAlpha:0}, {autoAlpha:1})
			},
		});

		tlOpening
			.fromTo('.hero--landing h1 span:nth-child(1)', 1.8, {autoAlpha:0, y:20}, {autoAlpha: 1, y:0, ease: Power2.easeOut},  2)
			.fromTo('.hero--landing h1 span:nth-child(3)', 1.8, {autoAlpha:0, y:40}, {autoAlpha: 1, y:0, ease: Power2.easeOut},  2.2)
			.fromTo('.hero--landing h2', 1, {autoAlpha: 0, y:20},{autoAlpha: 1, y:0}, 3)
			.addLabel('cta+=1')
			.fromTo('.hero--landing .cta', 0.35, {autoAlpha:0, y: 10}, {autoAlpha:1, y: 0}, 'cta')
			.fromTo('.hero--landing .hero-link', 0.35, {autoAlpha:0, y: 10}, {autoAlpha:1, y: 0}, 'cta')
			.to('header', 1, {autoAlpha:1}, 'cta')
			;



		// SCROLLING STUFF
		var controller = new ScrollMagic.Controller();


		// --- reveal steps on scrolling
		var tlRevealSteps = new TimelineMax({
			onComplete: function(){
				tlInsertCode.play(0);
				tlInsertNumber.play(0);
				tlScanId.play(0);
				tlSelfie.play(0);
			},
			onCompleteDelay: 4,
		}); 
		tlRevealSteps
			.from('.steps-container .box--grey', 1, {scaleY: 0, ease: Power3.easeInOut})
			.from('.box-title', 0.6, {y: 20, autoAlpha:0}, 0.45)
			.staggerFrom('.steps-container svg', 1, {y: 20, autoAlpha:0, ease:Power1.easeInOut}, 0.08, 0.4)
			.staggerFrom('.steps-container .smallTitle', 1, {autoAlpha:0, ease:Power1.easeInOut}, 0.08, 0.4)
			;

		var sceneSteps = new ScrollMagic.Scene({ // eslint-disable-line no-unused-vars
			triggerElement: '.steps-container',
			triggerHook: 0.8,
			reverse: false,
		})
		.setTween(tlRevealSteps)
		.addTo(controller);


		// --- reveal benefits on scrolling
		var tlRevealBenefits = new TimelineMax();
		tlRevealBenefits
			.from('.section-benefits .box--grey', 1, {scaleY: 0, ease: Power3.easeInOut})
			.staggerFrom('.section-benefits .col-md-4', 0.4, {y: 20, autoAlpha:0, ease: Power1.easeOut}, 0.15, 0.4)
			;

		var sceneBenefits = new ScrollMagic.Scene({ // eslint-disable-line no-unused-vars
			triggerElement: '.section-benefits .row.box',
			triggerHook: 0.8,
			reverse: false,
		})
		.setTween(tlRevealBenefits)
		.addTo(controller);


		// --- Team
		var tweenTeamBigText = TweenMax.fromTo('.section-team-bigText', 0.5, {xPercent: 0, autoAlpha:0}, {xPercent: 20, autoAlpha:1, ease: Power4.easeOut});

		var sceneTeamBigText = new ScrollMagic.Scene({ // eslint-disable-line no-unused-vars
			triggerElement: '.section-team',
			triggerHook: 1,
			duration: '100%',
			tweenChanges: true,
		})
		.setTween(tweenTeamBigText)
		.addTo(controller);

		function revealTeamScroll(){ // used in the callback of ajax call
			var tweenRevealTeam = TweenMax.staggerFrom('.team-member-container', 1, {autoAlpha:0, y:40, ease:Power1.easeOut}, 0);

			var sceneRevealTeam = new ScrollMagic.Scene({ // eslint-disable-line no-unused-vars
				triggerElement: '.team-member-container',
				triggerHook: 0.8,
				reverse: false,
			})
			.setTween(tweenRevealTeam)
			.addTo(controller)
		}





		// STEPS SVG ANIMATIONS
		var tlScanId = new TimelineMax({repeat: -1, paused: true, repeatDelay: 5});

		tlScanId
			.to('svg#scanId .frame:nth-child(1)', 0.2, {x: 5, y:5, repeat:1, yoyo:true}, 0)
			.to('svg#scanId .frame:nth-child(2)', 0.2, {x: -5, y:5, repeat:1, yoyo:true}, 0)
			.to('svg#scanId .frame:nth-child(3)', 0.2, {x: 5, y:-5, repeat:1, yoyo:true}, 0)
			.to('svg#scanId .frame:nth-child(4)', 0.2, {x: -5, y:-5, repeat:1, yoyo:true}, 0)
			;

		var tlSelfie = new TimelineMax({repeat: -1, paused: true, repeatDelay: 4.5});

		tlSelfie
			.to('svg#selfie .frame:nth-child(1)', 0.2, {x: 5, y:5, repeat:1, yoyo:true}, 0)
			.to('svg#selfie .frame:nth-child(2)', 0.2, {x: -5, y:5, repeat:1, yoyo:true}, 0)
			.to('svg#selfie .frame:nth-child(3)', 0.2, {x: 5, y:-5, repeat:1, yoyo:true}, 0)
			.to('svg#selfie .frame:nth-child(4)', 0.2, {x: -5, y:-5, repeat:1, yoyo:true}, 0)
			;

		var tlInsertCode = new TimelineMax({repeat: -1, paused: true, repeatDelay: 5});

		tlInsertCode
			.to('svg#insertCode .digit:nth-child(3)', 0.15, {autoAlpha: 0, repeat:1, yoyo: true})
			.to('svg#insertCode .digit:nth-child(5)', 0.15, {autoAlpha: 0, repeat:1, yoyo: true})
			.to('svg#insertCode .digit:nth-child(4)', 0.15, {autoAlpha: 0, repeat:1, yoyo: true})
			.to('svg#insertCode .digit:nth-child(2)', 0.15, {autoAlpha: 0, repeat:1, yoyo: true})
			.to('svg#insertCode .digit:nth-child(9)', 0.15, {autoAlpha: 0, repeat:1, yoyo: true})
			;

		var tlInsertNumber = new TimelineMax({repeat: -1, paused: true, repeatDelay: 5});

		tlInsertNumber
			.to('svg#insertNumber .number', 0.2, {autoAlpha:0})
			.staggerTo('svg#insertNumber .number', 0.15, {autoAlpha: 1}, 0.2)
			;	
	},



};
