import p5 from 'p5';  // eslint-disable-line no-unused-vars
var btcPath = require('../../images/btcLogo.jpg');
// var scamPath = require('../../images/scam2.jpg');

export default {
	init() {
		// JavaScript to be fired on the home page
	},
	finalize() {
		// JavaScript to be fired on the home page, after the init JS
		var windowWidth = $(window).width();
		var windowHeight = $(window).height();

		/* eslint-disable */
		p5.disableFriendlyErrors = true;
		var btc;
		var Stars = [];
		var Scams = [];
		var smallSize = 80;
		var scaleFactor = (windowWidth * 0.4) / smallSize;
		var starsColor = ['#1e5ed7', '#1d5cd2', '#2269e9'];




		var sketch = function(p){

			var bgColor = p.color('#000529');


			function Star(x,y){
				this.x = (x * scaleFactor) + 700;
				this.y = (y * scaleFactor) + 100;
				this.pos = p.createVector(700,300);
				this.target = p.createVector(this.x,this.y);
				this.color = p.random(starsColor);
				this.size = p.random(2,4);
				this.vel = p.createVector(-4,1);
				this.acc = p.createVector();
				this.maxSpeed = 5;
				this.maxForce = 2;

				this.alpha;
			}

			Star.prototype.update = function(){
				this.pos.add(this.vel);
				this.vel.add(this.acc);
				// this.vel.limit(this.maxSpeed);
				this.alpha;
				// this.acc.mult(0);
			}

			Star.prototype.move = function(){
				var attract = this.attract(this.target);
				this.appForce(attract);

				// var mouse = p.createVector(p.mouseX, p.mouseY);
				// var repulse = this.repulse(mouse);
				// this.appForce(repulse);

			}

			Star.prototype.appForce = function(value){
				this.acc.add(value);
			}

			Star.prototype.modAlpha = function(){
				var desired = p5.Vector.sub(this.target, this.pos);
				var d = desired.mag();
				var opacity = p.map(d, 0, 800, 255, 0);
				return opacity;
			}
			
			Star.prototype.attract = function(target){
				// var desired = p5.Vector.sub(target, this.pos);
				// var d = desired.mag();
				// var speed = 5;
				// // if( d < 100 ){
				// // 	speed = speed / 10;
				// // }
				// desired.setMag(speed);
				// var steer = p5.Vector.sub(desired, this.vel);
				// steer.limit(this.maxForce);
				// return steer;
				var force = (p5.Vector.sub(target, this.pos));
				// if(force.mag() < 60) {
				// 	force.setMag(0.3);
				// 	this.vel.limit(2)
				// } 
				var test = force.mag();
				var final = 1000 / (test * test);
				force.setMag(final);
				// force.limit(1);
				return force;

			}

			Star.prototype.repulse = function(target){
				var desired = p5.Vector.sub(target, this.pos);
				var d = desired.mag();
				if(d < 150) {
					desired.setMag(this.maxSpeed);
					desired.mult(p.random(-1,-0.5));
					var steer = p5.Vector.sub(desired, this.vel);
					steer.limit(this.maxForce * 5);
					return steer;					
				}
				else {
					return p.createVector(0,0);
				}
			}

			Star.prototype.render = function(){
				p.noStroke();
				var color = p.color(this.color);
				color.setAlpha(this.modAlpha());
				// p.fill(color);
				p.fill(p.color('white'));
				p.ellipse(this.pos.x, this.pos.y, this.size, this.size);					
			}


			p.setup = function(){
				p.createCanvas(windowWidth, windowHeight);
				p.pixelDensity(1);
				p.background(bgColor);
				var btc = p.loadImage(btcPath, function(img) {
					// p.image(img, 0, 0);
					btc.resize(smallSize,smallSize);
					btc.loadPixels();
					for( var x=0; x < btc.width; x++){
						for( var y=0; y < btc.height; y++){
							var index = (x + y * btc.width) * 4;
							if(btc.pixels[index] < 200){ // if channel red < 200 == black in this case
								if(Math.round(p.random(1000)) < 1){ //get only % of the points, randomly
									Stars.push(new Star(x,y));									
								}
							}
						}
					}
				}); // close callback loader image

				// var scam = p.loadImage(scamPath, function(img) {
				// 	// p.image(img, 0, 0);
				// 	scam.resize(smallSize,smallSize);
				// 	scam.loadPixels();
				// 	for( var x=0; x < scam.width; x++){
				// 		for( var y=0; y < scam.height; y++){
				// 			var index = (x + y * scam.width) * 4;
				// 			if(scam.pixels[index] < 200){ // if channel red < 200 == black in this case
				// 				var value = {
				// 					x: (x * scaleFactor) + 700,
				// 					y: (y * scaleFactor) + 100
				// 				}
				// 				Scams.push(value);
				// 			}
				// 		}
				// 	}
				// 	console.log(Scams);
				// }); // close callback loader image
				p.pixelDensity();				

			}



			p.draw = function(){
				p.background(bgColor);
				for(var k = 0; k < Stars.length; k++){
					Stars[k].move();					
					Stars[k].update();					
					Stars[k].render();
					p.fill(p.color('red'))
					p.ellipse(Stars[k].target.x,  Stars[k].target.y, 10, 10)
				}	
			}



			p.windowResized = function(){
				windowWidth = $(window).width();
				windowHeight = $(window).height();
				p.resizeCanvas(windowWidth, windowHeight);
			}


			// $('body').on('click', function(){
			// 	Stars.splice(Scams.length,Stars.length);

			// 	for(var k = 0; k < Scams.length; k++){
			// 		var rand = Math.round(p.random(Stars.length -1));
			// 		Stars[rand].target = p.createVector(Scams[k].x, Scams[k].y);
			// 	}

				
			// });
		};

		var yo = new p5(sketch, window.document.getElementById('container-p5'));

		/* eslint-enalbe */
	

	},
};
